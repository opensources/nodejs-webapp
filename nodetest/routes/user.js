
/*
 * GET users listing.
 */

exports.list = function(db){
  return function(req, res) {
        var collection = db.get('threepi_demo_users');
        collection.find({},{},function(e,docs){
            res.render('users', {
                "users" : docs
            });
        });
    };
};
